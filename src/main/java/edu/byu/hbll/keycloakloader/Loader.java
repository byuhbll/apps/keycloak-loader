package edu.byu.hbll.keycloakloader;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.impl.View;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.core.Response;
import lombok.Data;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.FederatedIdentityRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * A Box processor that follows Person and adds new users to Keycloak.
 */
@Component
public class Loader extends View {

  public static final String INSTITUTION = "byu";
  @Autowired private Keycloak keycloak;
  @Autowired private UncheckedObjectMapper mapper;
  @Autowired private Realms realms;

  @Override
  protected List<BoxDocument> transform(List<BoxDocument> documents) {
    return documents.stream().map(d -> process(d)).toList();
  }

  /**
   * Processes the user by attempting to create a corresponding Keycloak user for both the
   * institution specific realm as well as the main realm.
   *
   * @param person the user
   * @return the result of the processing
   */
  public BoxDocument process(BoxDocument personDocument) {
    String id = personDocument.getId();
    BoxDocument result = new BoxDocument(id);

    if (personDocument.isDeleted()) {
      result.setAsDeleted();
    } else {
      Person person = mapper.treeToValue(personDocument.getDocument(), Person.class);

      if (!person.getUsername().isBlank()) {
        createUser(INSTITUTION, person, INSTITUTION);
        createUser("ces", person, INSTITUTION + "-realm");
        result.setAsReady();
      }
    }

    return result;
  }

  /**
   * Creates a user if the user doesn't already exist.
   *
   * @param realmName the realm to use
   * @param person the person object
   * @param idpAlias which idp to connect the user to
   */
  public boolean createUser(String realmName, Person person, String idpAlias) {
    String username = person.getUsername() + "@" + INSTITUTION + ".edu";
    String email = username;
    String firstName = person.getPreferredName();
    String lastName = person.getSurname();
    Map<String, List<String>> attributes =
        Map.of(
            "byuId", List.of(person.getUserId()),
            "netId", List.of(person.getUsername()),
            "institution", List.of(INSTITUTION));
    List<String> realmRoles = person.getRoles();
    FederatedIdentityRepresentation federatedIdentity = new FederatedIdentityRepresentation();
    federatedIdentity.setIdentityProvider(idpAlias);
    federatedIdentity.setUserId(username);
    federatedIdentity.setUserName(username);

    if (idpAlias.contains("-realm")) {
      federatedIdentity.setUserId(
          getUser(idpAlias.replace("-realm", ""), username).orElseThrow().getId());
    }

    UserRepresentation user = new UserRepresentation();
    user.setUsername(username);
    user.setEmail(email);
    user.setEmailVerified(true);
    user.setFirstName(firstName);
    user.setLastName(lastName);
    user.setAttributes(attributes);
    user.setFederatedIdentities(List.of(federatedIdentity));
    user.setEnabled(true);

    // Unfortunately, setting the roles like this doesn't work due to an API bug:
    // https://stackoverflow.com/a/74221360/1530184
    // user.setRealmRoles(realmRoles);

    // do not add or update user if already exists
    if (getUser(realmName, username).isEmpty()) {
      Response response = keycloak.realm(realmName).users().create(user);

      if (response.getStatus() == HttpStatus.CONFLICT.value()) {
        // user already exists, ignore
        return false;
      }

      assignRoles(realmName, username, realmRoles);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Retrieves the Keycloak user given the realm and username.
   *
   * @param realmName the realm to use
   * @param username the username
   * @return the Keycloak user
   */
  public Optional<UserRepresentation> getUser(String realmName, String username) {
    List<UserRepresentation> results =
        keycloak.realm(realmName).users().searchByUsername(username, true);

    if (results.isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(results.get(0));
    }
  }

  /**
   * Because of an API bug, assigning roles is not as simple as adding them to the user
   * representation when creating a new user. Until the bug is fixed, it must be done this way.
   *
   * <p>https://stackoverflow.com/a/74221360/1530184
   *
   * @param realmName
   * @param username
   * @param realmRoles
   */
  public void assignRoles(String realmName, String username, List<String> realmRoles) {
    UserRepresentation user = getUser(realmName, username).orElseThrow();
    UserResource userResource = keycloak.realm(realmName).users().get(user.getId());
    final Map<String, RoleRepresentation> realmRoleMap = realms.getRealmRoles(realmName);
    List<RoleRepresentation> realmRoleReps =
        realmRoles.stream().map(r -> realmRoleMap.get(r)).toList();
    userResource.roles().realmLevel().add(realmRoleReps);
  }

  /** The person object as found at the Person service. */
  @Data
  public static class Person {
    private String userId;
    private String username;
    private String preferredName;
    private String surname;
    private List<String> roles;
  }
}
