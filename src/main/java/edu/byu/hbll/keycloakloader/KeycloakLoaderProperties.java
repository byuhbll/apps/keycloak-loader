package edu.byu.hbll.keycloakloader;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** Configuration properties for the application. */
@Configuration
@ConfigurationProperties("byuhbll.keycloak-loader")
@Data
public class KeycloakLoaderProperties {

  private KeycloakProperties keycloakAdmin;

  @Data
  public static class KeycloakProperties {
    private String baseUri;
    private String clientId;
    private String clientSecret;
  }
}
