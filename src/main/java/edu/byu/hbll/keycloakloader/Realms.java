package edu.byu.hbll.keycloakloader;

import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/** A component for retrieving and caching realm roles. */
@Component
@Slf4j
@AllArgsConstructor
public class Realms {

  private final Keycloak keycloak;

  /**
   * Retrieve the realm roles from Keycloak and cache them.
   *
   * @param realmName the realm name
   * @return the realm roles
   */
  @Cacheable("realmRoles")
  public Map<String, RoleRepresentation> getRealmRoles(String realmName) {
    return keycloak.realm(realmName).roles().list().stream()
        .collect(Collectors.toMap(r -> r.getName(), r -> r));
  }

  /** Regularly clears the realm role cache. */
  @CacheEvict(value = "realmRoles", allEntries = true)
  @Scheduled(cron = "@hourly")
  public void emptyRealmRolesCache() {
    log.debug("cleared realm cache");
  }
}
