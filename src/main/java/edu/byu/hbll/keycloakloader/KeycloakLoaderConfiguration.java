package edu.byu.hbll.keycloakloader;

import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.keycloakloader.KeycloakLoaderProperties.KeycloakProperties;
import lombok.AllArgsConstructor;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/** Spring bean configuration for the application. */
@Configuration
@EnableCaching
@EnableScheduling
@AllArgsConstructor
public class KeycloakLoaderConfiguration {

  private KeycloakLoaderProperties properties;

  @Bean
  public UncheckedObjectMapper objectMapper() {
    return ObjectMapperFactory.newUnchecked();
  }

  @Bean
  public Keycloak keycloak() {
    KeycloakProperties keycloakAdmin = properties.getKeycloakAdmin();

    return KeycloakBuilder.builder()
        .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
        .serverUrl(keycloakAdmin.getBaseUri())
        .realm("master")
        .clientId(keycloakAdmin.getClientId())
        .clientSecret(keycloakAdmin.getClientSecret())
        .build();
  }
}
