package edu.byu.hbll.keycloakloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** The entry point for the application. */
@SpringBootApplication
public class KeycloakLoaderApplication {

  /**
   * Launches this application.
   *
   * @param args the command line arguments provided at runtime
   */
  public static void main(String[] args) {
    SpringApplication.run(KeycloakLoaderApplication.class, args);
  }
}
